@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('Role Edit') }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Role</a></li>
                <li class="breadcrumb-item active">{{ __('Role Edit') }}</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 margin-tb">
                                <h3 class="card-title">{{ __('Edit Role') }}</h3>
                            </div>
                            <div class="col-lg-6 col-md-6 margin-tb">
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal"action="{{ route('roles.update', $role->id) }}" method="POST">
    	                @csrf
                        @method('PUT')
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $role->name ?? old('name') }}" required autocomplete="name" autofocus placeholder="Name">

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="permission" class="col-sm-2 col-form-label">Permission</label>
                                <div class="col-sm-10">
                                    <select class="select2 @error('permission') is-invalid @enderror" data-placeholder="Select a Permission" name="permission[]" multiple="multiple" data-placeholder="Select a State" style="width: 100%;">

                                    @foreach($permission as $value)
                                            <option value="{{$value->id}}" @if(in_array($value->id, $rolePermissions)) selected @endif>{{ $value->name }}</option>
                                    @endforeach
                                    </select>

                                    @error('roles')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info">Save</button>
                            <a href="{{ route('roles.index')}}" class="btn btn-default float-right">Cancel</a>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php // dd($permission->toArray());// $m_array = preg_grep('/^strtok("$value->name",  '-')/', (array)$permission); ?>
@endsection
