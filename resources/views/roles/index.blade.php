@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('Role Management') }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">{{ __('Role Management') }}</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 margin-tb">
                                <h3 class="card-title">{{ __('Role List') }}</h3>
                            </div>
                            <div class="col-lg-6 col-md-6 margin-tb">
                                <div class="pull-right" style="float: right;">
                                @can('role-create')
                                    <a class="btn btn-primary" href="{{ route('roles.create') }}"> Create New Role</a>
                                @endcan
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th width="280px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($roles as $key => $role)
                                        <tr>
                                            <th scope="row">{{ $role->id }}</th>
                                            <td>{{ $role->name }}</td>
                                            <td>
                                                <a class="btn btn-info" href="{{ route('roles.show',$role->id) }}">Show</a>
                                                <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">Edit</a>
                                                <a class="btn btn-danger delete" value="{{ $role->id }}">Delete</a>

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    {{-- Pagination --}}
                    {!! $roles->links('pagination.custom-pagination') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>

    $(".delete").on('click', function(event){
        let role_id = $(".delete").attr('value');
        swal({
        title: "Are you sure?",
        text: "You want to delete this role!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
        },
        function(isConfirm) {
        if (isConfirm) {
            $.ajax({
            method: "DELETE",
            url: '{{ url("roles") }}/' +  role_id,
            data: {
                "_token": "{{ csrf_token() }}",
            },
            success: function(data)
            {
                swal({
                    title: "Deleted!",
                    text: "Role has been deleted.",
                    type: "success",
                    confirmButtonText: "Ok",
                    timer: 3000,
                    allowOutsideClick: "true"
                }, function () { location.reload(); })
                // swal("Deleted!", "Your product has been deleted.", "success");
                // show response from the php script.
            }
            });

        } else {
            swal("Cancelled", "Role is safe :)", "error");
        }
        });
    });

</script>
@endsection