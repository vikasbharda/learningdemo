@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('User Management') }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">{{ __('User Management') }}</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 margin-tb">
                                <h3 class="card-title">{{ __('User List') }}</h3>
                            </div>
                            <div class="col-lg-6 col-md-6 margin-tb">
                                @can('user-create')
                                <div class="pull-right" style="float: right;">
                                    <a class="btn btn-primary" href="{{ route('users.create') }}"> Create New User</a>
                                </div>
                                @endcan
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Roles</th>
                                    <th width="280px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($userData as $key => $user)
                                        <tr>
                                            <th scope="row">{{ $user->id }}</th>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>
                                                @if(!empty($user->getRoleNames()))
                                                    @foreach($user->getRoleNames() as $v)
                                                    <label class="badge badge-success">{{ $v }}</label>
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td>
                                                <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Show</a>
                                                <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>
                                                <a class="btn btn-danger delete" value="{{ $user->id }}">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    {{-- Pagination --}}
                    {!! $userData->links('pagination.custom-pagination') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>

    $(".delete").on('click', function(event){
        let user_id = $(".delete").attr('value');
        swal({
        title: "Are you sure?",
        text: "You want to delete this user!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
        },
        function(isConfirm) {
        if (isConfirm) {
            $.ajax({
            method: "DELETE",
            url: '{{ url("users") }}/' +  user_id,
            data: {
                "_token": "{{ csrf_token() }}",
            },
            success: function(data)
            {
                swal({
                    title: "Deleted!",
                    text: "User has been deleted.",
                    type: "success",
                    confirmButtonText: "Ok",
                    timer: 3000,
                    allowOutsideClick: "true"
                }, function () { location.reload(); })
                // swal("Deleted!", "Your product has been deleted.", "success");
                // show response from the php script.
            }
            });

        } else {
            swal("Cancelled", "User is safe :)", "error");
        }
        });
    });

</script>
@endsection
