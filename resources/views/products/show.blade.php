@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('Products View') }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Products</a></li>
                <li class="breadcrumb-item active">{{ __('Products View') }}</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 margin-tb">
                                <h3 class="card-title">{{ __('View Product') }}</h3>
                            </div>
                            <div class="col-lg-6 col-md-6 margin-tb">
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="{{ route('products.update',$product->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="productName" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="productName" class="form-control" value="{{ $product->productName }}" placeholder="Name" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="productPrice" class="col-sm-2 col-form-label">Price</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="productPrice" value="{{ $product->productPrice }}" placeholder="Price" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="productDescription" class="col-sm-2 col-form-label">Brand</label>
                                <div class="col-sm-10">
                                    <select class="custom-select" name="brand_id" disabled>
                                    @foreach($brands as $brand)
                                        <option value="{{$brand->id}}" @if($brand->id == $product->brand_id) selected @endif>{{ $brand->brandName }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="productDescription" class="col-sm-2 col-form-label">Category</label>
                                <div class="col-sm-10">
                                    <select class="custom-select" name="category_id" disabled>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}" @if($category->id == $product->category_id) selected @endif>{{ $category->categoryName }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="productDescription" class="col-sm-2 col-form-label">Detail</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" style="height:150px" name="productDescription" placeholder="Detail" disabled> {{ $product->productDescription }} </textarea>
                                </div>
                            </div>

                            <input type="hidden" name="productStatus" class="form-control" placeholder="Product Status" value="1">
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <a href="{{ route('products.index')}}" class="btn btn-default float-right">Back</a>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
