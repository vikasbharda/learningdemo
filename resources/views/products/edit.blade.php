@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('Products Edit') }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Products</a></li>
                <li class="breadcrumb-item active">{{ __('Products Edit') }}</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 margin-tb">
                                <h3 class="card-title">{{ __('Edit Product') }}</h3>
                            </div>
                            <div class="col-lg-6 col-md-6 margin-tb">
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="{{ route('products.update',$product->id) }}" method="POST" enctype='multipart/form-data'>
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="productName" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="productName" class="form-control" value="{{ $product->productName }}" placeholder="Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="productPrice" class="col-sm-2 col-form-label">Price</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="productPrice" value="{{ $product->productPrice }}" placeholder="Price">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="productDescription" class="col-sm-2 col-form-label">Brand</label>
                                <div class="col-sm-10">
                                    <select class="custom-select" name="brand_id">
                                    @foreach($brands as $brand)
                                        <option value="{{$brand->id}}" @if($brand->id == $product->brand_id) selected @endif>{{ $brand->brandName }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="productDescription" class="col-sm-2 col-form-label">Category</label>
                                <div class="col-sm-10">
                                    <select class="custom-select" name="category_id">
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}" @if($category->id == $product->category_id) selected @endif>{{ $category->categoryName }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="productDescription" class="col-sm-2 col-form-label">Detail</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" style="height:150px" name="productDescription" placeholder="Detail"> {{ $product->productDescription }} </textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputFile" class="col-sm-2 col-form-label">Image</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input file-input" name="images[]" id="exampleInputFile" multiple>
                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row img-preview">
                                @foreach($product->product_images as $val)
                                <div class="col-md-2">
                                    <img src="{{ asset($val->image)}}" class="img-fluid img-thumbnail" alt="image">
                                </div>
                                @endforeach
                            </div>
                            <input type="hidden" name="productStatus" class="form-control" placeholder="Product Status" value="1">
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info">Save</button>
                            <a href="{{ route('products.index')}}" class="btn btn-default float-right">Cancel</a>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script>

    function previewImages() {

        var $preview = $('.img-preview');
        if (this.files) $.each(this.files, readAndPreview);

        function readAndPreview(i, file) {

        if (!/\.(jpe?g|png|gif)$/i.test(file.name)){

            return swal("Opps!", file.name + " is not an image.", "warning");
        } // else...

        var reader = new FileReader();

        $(reader).on("load", function() {
            $preview.append(
                '<div class="col-md-2">' +
                    '<img src="'+ this.result +'" class="img-fluid img-thumbnail" alt="image">' +
                '</div>'
            ); //$("<img/>", {src:this.result, height:100})
        });

        reader.readAsDataURL(file);

        }

    }
    $('.file-input').on("change", previewImages);
</script>
@endsection
