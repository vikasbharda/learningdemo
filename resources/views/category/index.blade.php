@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('Category Management') }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">{{ __('Category Management') }}</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 margin-tb">
                                <h3 class="card-title">{{ __('Category List') }}</h3>
                            </div>
                            <div class="col-lg-6 col-md-6 margin-tb">
                                @can('category-create')
                                <div class="pull-right" style="float: right;">
                                    <a class="btn btn-primary" href="{{ route('categories.create') }}"> Create New Category</a>
                                </div>
                                @endcan
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Details</th>
                                    <th width="280px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($categories as $category)
                                        <tr>
                                            <th scope="row">{{ $category->id }}</th>
                                            <td>{{ $category->categoryName }}</td>
                                            <td>{{ $category->categoryDescription }}</td>
                                            <td>
                                                <a class="btn btn-info" href="{{ route('categories.show',$category->id) }}">Show</a>
                                                <a class="btn btn-primary" href="{{ route('categories.edit',$category->id) }}">Edit</a>
                                                <a class="btn btn-danger delete" value="{{ $category->id }}">Delete</a>

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    {{-- Pagination --}}
                    {!! $categories->links('pagination.custom-pagination') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>

    $(".delete").on('click', function(event){
        let category_id = $(".delete").attr('value');
        swal({
        title: "Are you sure?",
        text: "You want to delete this category!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
        },
        function(isConfirm) {
        if (isConfirm) {
            $.ajax({
            method: "DELETE",
            url: '{{ url("categories") }}/' +  category_id,
            data: {
                "_token": "{{ csrf_token() }}",
            },
            success: function(data)
            {
                swal({
                    title: "Deleted!",
                    text: "Your category has been deleted.",
                    type: "success",
                    confirmButtonText: "Ok",
                    timer: 3000,
                    allowOutsideClick: "true"
                }, function () { location.reload(); })
                // swal("Deleted!", "Your category has been deleted.", "success");
                // show response from the php script.
            }
            });

        } else {
            swal("Cancelled", "Your category is safe :)", "error");
        }
        });
    });

</script>
@endsection