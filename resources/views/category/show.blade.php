@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ __('Category View') }}</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Category</a></li>
                <li class="breadcrumb-item active">{{ __('Category View') }}</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 margin-tb">
                                <h3 class="card-title">{{ __('View Category') }}</h3>
                            </div>
                            <div class="col-lg-6 col-md-6 margin-tb">
                            </div>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form class="form-horizontal" action="{{ route('categories.update',$category->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="categoryName" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="categoryName" class="form-control" value="{{ $category->categoryName }}" placeholder="Name" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="parent_id" class="col-sm-2 col-form-label">Category</label>
                                <div class="col-sm-10">
                                    <select class="custom-select" name="parent_id" disabled>
                                    @foreach($categories as $data)
                                        <option value="{{$data->id}}" @if($data->id == $category->parent_id) selected @endif>{{ $data->categoryName }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="categoryDescription" class="col-sm-2 col-form-label">Detail</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" style="height:150px" name="categoryDescription" placeholder="Detail" disabled> {{ $category->categoryDescription }} </textarea>
                                </div>
                            </div>

                            <input type="hidden" name="categoryStatus" class="form-control" placeholder="Category Status" value="1">
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <a href="{{ route('categories.index')}}" class="btn btn-default float-right">Back</a>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection