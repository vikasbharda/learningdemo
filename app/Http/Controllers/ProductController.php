<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductReview;
use App\Models\Brand;
use App\Models\Category;
use App\Models\ProductImage;
use Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:product-list|product-create|product-edit|product-delete', ['only' => ['index','show']]);
        $this->middleware('permission:product-create', ['only' => ['create','store']]);
        $this->middleware('permission:product-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:product-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::latest()->paginate(2);
        return view('products.index',compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::where('brandStatus',1)->get();
        $categories = Category::where('categoryStatus',1)->get();
        return view('products.create',compact('brands','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'productName' => 'required',
            'brand_id' => 'required',
            'category_id' => 'required',
            'productDescription' => 'required',
            'productPrice' => 'required|numeric',
        ]);

        Product::create($request->all());

        return redirect()->route('products.index')
                        ->with('success','Product created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $brands = Brand::where('brandStatus',1)->get();
        $categories = Category::where('categoryStatus',1)->get();
        return view('products.show',compact('product','brands','categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $brands = Brand::where('brandStatus',1)->get();
        $categories = Category::where('categoryStatus',1)->get();
        return view('products.edit',compact('product','brands','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'brand_id' => 'required',
            'category_id' => 'required',
            'productName' => 'required',
            'productDescription' => 'required',
            'productPrice' => 'required|numeric',
        ]);

        $product = Product::find($id);
        $product->update($request->all());

        $filePath = "uploads/products/$product->id";

        if(!empty($request->images)){
            foreach($request->images as $key => $image) {
                $extension = $request->images[$key]->extension();
                $fileName = "product-" . uniqid() . '.' . $extension;
                $url = "$filePath/$fileName";

                $upload_image = Storage::disk('public_uploads')->put($url, file_get_contents($image), 'public');

                if($upload_image) {
                    $imageData = ['product_id' => $product->id, 'image' => $url];
                    $productImage = ProductImage::create($imageData);
                }
            }
        }

        return redirect()->route('products.index')
                        ->with('success','Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      //  $product = ProductReview::where('product_id',$id)->delete();
        $product = Product::findOrFail($id);
        $product->delete();

        return true;
    }
}
