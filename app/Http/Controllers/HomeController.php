<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function showProfile(){
        $userInfo = Auth()->user();

        return view('profile',compact('userInfo'));
    }

    public function updateProfile(Request $request){
        $request->validate([
            'name' => 'required|string|max:255',
        ]);

        $updateProfile = User::whereId(Auth()->user()->id)->update(['name' => $request->name]);

        return back()->with('status', 'Your Profile has been saved successfully.');;
    }

    public function updatePassword(Request $request){
        $request->validate([
            'current_password'      => ['required', new CheckCurrentPassword],
            'new_password'          => 'required|min:8',
            'password_confirmation' => 'required|same:new_password',
        ]);

        $updatePassword = User::whereId(Auth()->user()->id)->update(['password' => Hash::make($request->new_password)]);

        return back()->with('password_status', 'Your Password has been changed successfully.');;
    }
}
