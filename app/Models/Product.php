<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Brand;
use App\Models\Category;
use App\Models\ProductReview;
use App\Models\ProductImage;

class Product extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'brand_id',
        'category_id',
        'productName',
        'productDescription',
        'productPrice',
        'productStatus',
    ];

    /**
     * Get the user that owns the phone.
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class,'brand_id');
    }

    /**
     * Get the user that owns the phone.
     */
    public function category()
    {
        return $this->belongsTo(Category::class,'category_id');
    }

    /**
     * Get the user that owns the phone.
     */
    public function product_images()
    {
        return $this->hasMany(ProductImage::class,'product_id');
    }

    /**
     * Get the user that owns the phone.
     */
    public function reviews()
    {
        return $this->hasMany(ProductReview::class);
    }

}
